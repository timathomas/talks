# Testimony before the WA State Senate Committee on Housing Stability and Affordability

Tim Thomas - t77@uw.edu
January 16th, 2019

## Slide 1 - 
Dear committee, I am honored and thank you for the chance to speak to you today. My name is Dr. Tim Thomas and I’m a postdoctoral researcher at the University of Washington's Department of Sociology and the eScience Institute. My research focuses on migration, economic stratification, and segregation. I'm a first generation PhD that grew up in the South, moved to Seattle in 2003, and worked in construction as a land surveyor for 8 years. So my work is not purely academic, but also informed by my experiences growing up working class in the South and the Pacific Northwest. 

I've been asked to talk about my research on housing, mobility, and neighborhood changes in racial and ethnic composition across Washington State. While most of my work focuses on King County, the urban and rural contexts are translatable to other parts of the state. 

## Slide 2 - Summary
I'll start with a brief history of neighborhood formation, which helps explain current shifts in racial compositions in Washington, followed by several mechanisms that explain mobility through economic stratification and forced mobility. Finally, I would like to share some policy recommendations based on my observations and work conducted by colleagues at the Department of Real Estate at the University of Washington. 

## Slide 3 - Seattle Race
We really can't talk about the current state of housing and racial compositions without first understanding how neighborhoods were formed. 

This map shows the racial composition and segregation of Seattle in 2010. The red dots represent White individuals, while blue represent African Americans, green represents Asian, and Yellow represents Latinx. 
Very quickly, you can see distinct separations between these groups. 

This structure is not random. Rather, it is shaped by political, social, and economic conditions over history. 

## Slide 4 - Redlining
For example, banks and policy makers used red lining to force communities of color to live in less desirable and hazardous portions of the city. This prevented households to participate in the growth of the middle class during the 20th century. 

## Slide 5 - black concentration
As Southern black households moved to the West during the great migration, they were forced into segregation where concentrated poverty, unemployment, and other discriminatory practices followed. 

## Slide 6 - Japanese internment
Another example that influenced neighborhood change is the Japanese internment of 1942 where Japanese US Citizens were sent to internment camps during World War 2. 

## Slide 7 - Japanese Internment
Upon their return, their homes were re-purposed and their neighborhoods rezoned, forcing them to move to other portions of the city. With over 400 housing covenants in King County preventing access to white segregated neighborhoods, they moved south with other Asian Americans to form what we now know as the International district. 

The success of the Civil Rights and the Fair Housing Acts of 1968 allowed households of color to move out of segregated and poor neighborhoods and seek economic opportunities elsewhere. 

However, the economic toll of segregation and continuation of informal discrimination by neighbors, lenders, and policymakers posed a major hurdle to economic success and mobility for households of color. 

## Slide 8 - Puget Sound change

By the 1990's and early 2000’s, a new era of urban revitalization, otherwise known as  gentrification, took place in urban centers. Lenders started providing loans that brought much needed capital, revitalizing and rebuilding segregated neighborhoods. 

However, loans in the urban center went mostly to white households, while households with poor credit were given sub-prime loans——we all know what happened after that. While White and Asian households suffered serious consequences of deregulated and shady lending, leading to the great recession, we now know through analyzing HMDA data that Black and Latinx families and neighborhoods were among the hardest hit. 

These maps show this inversion in the racial composition over the past 40 years. Here we see that households of color have been able to move into the outskirts of Olympia, Tacoma, Seattle, and parts of Kitsap County. Red in the maps shows a decrease in the percentage point change in race or ethnicity from 1980 to 2010 while blue shows growth. 

Now, I need to note that limitations in US Census data makes it difficult to show all groups. So I can only speak confidently about patterns among White, Asian, Latinx, and African American individuals. 

On the far left we see that the once majority white composition across the Puget Sound is now shared with Asian, Latinx, and Black households, where the largest change in composition seems to occur in South King and North Pierce Counties. For Asian residents in the second tile, the darker blue shows a compositional increase in areas that were historically White segregated in the past, but fairly even growth occurred across most parts of the Puget Sound. 

Latinx and Black residents, on the other hand, seem to be growing in limited areas. In fact, areas with no growth occurring in spaces that had most of the historical housing covenants. Black residents, in particular, are seeing large decreases in formerly segregated urban spaces that are now gentrifying with upwards of 50 to 90 percentage point declines in the composition of those red neighborhoods. This shift relates to the stories of displacement we've heard about over the past decade. 

While these trends may suggest racial diversity, research by my colleagues and I paints a different picture where we find that segregation still persists at the block level and gentrification is widening economic differences between white, Latinx, and Black families. I can speak more on this in the question and answer time. 

## Slide 9 - Washington
Moving out to the whole state, the most noticeable trend is the increase in the share of Latinx residents in the historically majority white Yakima Valley. Asian and Black households largely stayed in the Puget Sound region. 

While these maps are informative, and we see evidence of gentrification in the urban areas pushing households out of the city, it's still a little unclear what makes people move. We know that the cost of moving is high. Research tells us that a majority of residents tend to make short-distance moves, such as across town or even the other side of a neighborhood. Maybe the rent got too high or, worse, maybe a single mother was evicted from her home. Those entering retirement may use their resources to buy that house on Lopez Island or Port Townsend. A young tech worker may move to Seattle to work at Amazon while migrant farm workers move to Eastern Washington for the harvest. 

Whatever the case, these scenarios describe a range of mobility patterns that is either by choice or by force. And where individuals end up directly, and indirectly, impacts their opportunities and livelihood.

## Slide 10 - Mobility Curve
This slide shows a theoretical mobility curve where we have basically three groups of people: Those that are able to move by choice on the right, those that cannot move due to economic or social reasons, and those that are forced to move. On the x-axis is economic status that increases as you move right. 

We know a lot about people in the middle and on the right. They are the wealthy, probably homeowner or renting by choice, and then those living just  at their economic means in the middle with few options or reasons to move. However, we don't know a lot about those that are forced to move on the left-hand side. This left-third category consists largely of renters who have lower wages or some kind of cost-burden. They may be informally forced to move because the rent just went up and they found a cheaper place nearby or they were evicted for falling behind on rent. 

## Slide 11 - Fair market rent

Prior research finds that forced mobility occurs when stagnant and low wages fail to compete with increasing rent. This slide shows change in the average fair market rent (or FMR) for all bedroom times from 2000 to 2019 using the Department of Housing and Urban Development's estimates across the state and in the top five most populated counties. 

On the left hand side is rent adjusted for inflation using 2017 dollars while the x axis shows the year. The right hand side of the graph shows how much a household would need to earn to avoid rent burden, which is defined as giving  30% or more of your income to rent. 

As you can see, several counties have seen quite an increase in rent since 2000. The orange line at the bottom shows the average FMR for all of Washington starting at \$800 and landing around \$1000 in 2019. On average, a Washington household needs to make \$40,000 to avoid being rent burdened. King and Snohomish in Blue at the top has seen the most drastic increase up to an average of \$2,200 in rent requiring a household to earn about \$90,000 to avoid rent burden. Since the year 2000, King and Snohomish went up in rent by about \$1,000. In Clark county you need \$65,000 to afford rent and about \$55,000 in Pierce county. 

## Slide 12 - Income
To get a better idea of who is able to afford these costs, we need to look at the racial differences in median household income from 2000 to 2017. 

This graph shows Washington State median income, adjusted for inflation, where the green line shows the overall median household income which is sitting just under 70,000 dollars. This means that half of the state's households are below this line while the other half are above. You can see that White median household income, represented by the red line, is just above the overall median, while Asian income in orange is further above. Latinx and Black income is resting at 50,000 dollars, about 20,000 dollars less than the overall median. 

The grey bands show what is considered low income at the top, very low income in the middle, and extremely low income at the bottom based on the area median income. You'll notice that more than half of Latinx and Black households fall below the low-income mark across time. This gap between these groups and White median income is tied to the legacy of segregation and income inequality. 

## Slide 13 - King added income
King County on the right shows a very large gap where Black households fall along the very low-income band, which is about 55,000 dollars lower than white and Asian households who have a median income just under 100,000 dollars in 2017.

## Slide 14 - Evictions
As I mentioned before, households that are forced to move are largely renters. There is no publicly available data on families that moved because rent got too high but, we do have some data on formal evictions. A recent study in Seattle showed that a large number of people evicted last year owed only 100 dollars in late rent. The research on evictions also shows that it has long lasting effects on health, income, employability, and getting affordable housing in the future. It is also one of the major contributors to homelessness. 

As you can see from this graph, thousands of people are formally evicted every year. The decline in evictions you see in King County may not be as promising as it looks. A recent study by the urban institute on Washington DC saw a similar declining trend, but that was because they basically evicted everyone that they already could. While DC gained rental units, it lost some 30,000 affordable housing units. Evicted households are mostly low-income so when affordable housing is lost, they have the option to move out of the area, or even the city, or enter into homelessness. There's just no where to move given the cost of housing.

## Slide 15 - affordable housing
I tested this theory for Washington and between 2012 and 2017, only 6 years, we lost about 80,000 affordable rental units at \$800 a month or less, adjusted for inflation, while gaining over 100,000 new rentals since over 17 years. 

## Slide 16 - homelessness
Looking at the yearly point in time count for individuals that are homeless, we see an increase in homelessness starting in 2013, just one year after we started losing affordable housing in 2012. In 2018, we matched the number of individuals that were homeless during the great recession. One thing to note is that this is not due to in-migration of un-housed individuals. The last point in time count survey on homelessness in King County found that 80% of those experiencing homelessness were from the county and 90% from the State of Washington. So, the myth of the migrant homeless doesn't hold up, these are our people. In addition, this number severely undercounts actual homelessness because it does not account for people doubling up with friends or families. 

## Slide 17 - evictions 

When we stack on evictions, we see that Washington had a pretty steady number of evictions since 2009. The problem with this is that with declining affordable housing, and the fact that a record of an eviction inhibits your chances of getting new housing, then the steady stream of evictions each year likely feeds families into the homeless population. 

## Slide 18 - King

Finally, when we look at King County's decline in evictions, it seems plausible that the sharp decrease in affordable housing and the sharp increase in homelessness, could very well mean that we have evicted every low income household we could in that area. 

## Slide 19 - female to male ratio 

My team of researchers have been busy analyzing evictions over the past 6 months and we find that women are evicted most in Washington. This graph shows the female to male ratio of evicted adults where anything above 1 means more women were evicted while anything below 1 means more men were evicted. In King and Snohomish, more men were evicted, but only by about 2% on average. In all the rest of the counties in Washington, women were evicted upwards of 20% more than men in their respective county. 

In earlier research, I found that black women were evicted 7 times more than white women in King County. 

## Slide 20 - race

Our most shocking finding is in the racial disparity in evictions. In King County on the left 11% of all black adults were evicted between 2012 and 2017. That's 1 in 10. In Pierce, 21%, which is a staggering 1 in 5 black adults were evicted. In Whatcom, there are very few evictions and very little diversity. In other words, it seems that wherever there's a substantial black population, they are over-represented in evictions. 

## Slide 21 - neighborhood diversity

In some of my earlier work on my dissertation, I found that diverse neighborhoods have the highest rates of eviction, with Black households seeing the highest rates. 

## Slide 22 - rent 

and we see that the lowest rent areas have the highest rates overall. 

To a large degree. These trends, and subsequent changes in the racial composition across the puget sound are due to a legacy of discrimination that forced poor households to unsuccessfully fight the brunt of gentrification. This, in turn, is perpetuating inequality in urban spaces and forcing families into housing instability. 

What's problematic about forced mobility is that it disrupts families, moves individuals away from opportunity, good neighborhoods, good schools, and puts families at a severe disadvantage to improve their livelihood. 

**It seems that gentrification is picking up where redlining left off 50 years ago**

## Slide 23 - Summary
 
## Slide 24 - Policy recommendations 

## Slide 25 - Thank you
